package com.reltio.scripts.customer;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.reltio.cst.exception.handler.APICallFailureException;
import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.service.TokenGeneratorService;
import com.reltio.cst.service.impl.SimpleReltioAPIServiceImpl;
import com.reltio.cst.service.impl.TokenGeneratorServiceImpl;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class FindDuplicateCrosswalkAttributes {
    
    private static Gson gson = new Gson();
    public static void main(String[] args) throws Exception {
    
        System.out.println("Process Started..");
        long startTime = System.currentTimeMillis();
        Properties properties = new Properties();
        ReltioScanService reltioScanService;
    
        try {
            String propertyFilePath = args[0];
            FileReader fileReader = new FileReader(propertyFilePath);
            properties.load(fileReader);
        } catch (Exception e) {
            System.out.println("Failed to Read the Properties File :: ");
            e.printStackTrace();
        }
    
        final String api = properties.getProperty("API_URL");
        final String entityType = properties.getProperty("ENTITY_TYPE");
        final String username = properties.getProperty("USERNAME");
        final String password = properties.getProperty("PASSWORD");
        final String output = properties.getProperty("OUTPUT");
        final String scanFilter = properties.getProperty("SCAN_FILTER");
        final String maxCount = properties.getProperty("MAX_COUNT");
        final String tasksApi = properties.getProperty("TASKS_API");
        final String maxPages = properties.getProperty("MAX_PAGES");
        int entityCount = 0;
        final AtomicInteger foundCount = new AtomicInteger(0);
        

    
        final BufferedWriter b = new BufferedWriter(new FileWriter(output));
        
        
        final String SCAN_URL = "entities/_scan?select=uri&max="+maxCount+"&activeness=active&options=ovOnly&scoreEnabled=false&select=uri";
        final String scanUrl = api + SCAN_URL;
        String initialJSON = null;
    
        reltioScanService = new ReltioScanService();
        reltioScanService.setPageSize(Integer.parseInt(maxCount));
        reltioScanService.scan(scanFilter);
        reltioScanService.setApiUrl(api);
        reltioScanService.setReturnUriOnly(true);
        reltioScanService.setMaxPages(maxPages == null || maxPages.isEmpty() ? Integer.MAX_VALUE : Integer.parseInt(maxPages));
        TokenGeneratorService tokenGeneratorService = null;
        try {
            tokenGeneratorService = new TokenGeneratorServiceImpl(username, password, "https://auth.reltio.com/oauth/token");
        } catch (APICallFailureException e) {
            System.out.println("Error Code: " + e.getErrorCode() + " >>>> Error Message: " + e.getErrorResponse());
        } catch (GenericException e) {
            System.out.println(e.getExceptionMessage());
        }
        final ReltioAPIService reltioAPIService = new SimpleReltioAPIServiceImpl(tokenGeneratorService);
    
        final int[] count = {0};
        boolean eof = false;
        int threadsNumber = 100;
        ExecutorService executorService = Executors.newFixedThreadPool(threadsNumber);
        int scans = 1;
        int duplicateCount = 0;
        while (reltioScanService.hasNext(reltioAPIService)) {
                List<Map<String,Object>> objects = reltioScanService.getNext(reltioAPIService);
                    if (objects != null && objects.size() > 0) {
                        System.out.println("Retrieved entities for duplicate HCPUniqueId...[ScanCount= "+scans+", entityCount = "+objects.size());
                        entityCount += objects.size();
                        executorService.submit(() -> {
                            try {
                                for (Map<String, Object> entity : objects) {
                                    String uri = entity.get("uri").toString();
                                    JsonObject attributes = new Gson().toJsonTree(entity.get("attributes")).getAsJsonObject();
                                    JsonArray crosswalks = new Gson().toJsonTree(entity.get("crosswalks")).getAsJsonArray();
                                    JsonArray hcpUniqueIdList = attributes.getAsJsonArray("HCPUniqueId");
                                    Iterator<JsonElement> hcpUniqueIdIt = hcpUniqueIdList.iterator();
                                    Map<String,String> map = Maps.newHashMap();
                                    Map<String,String> hcpUniqueCrosswalkMap = Maps.newHashMap();
                                    Map<String, List<String>> duplicateCws = Maps.newHashMap();
                                    while(hcpUniqueIdIt.hasNext()) {
                                        JsonObject hcpUniqueId = hcpUniqueIdIt.next().getAsJsonObject();
                                        map.put(hcpUniqueId.get("uri").getAsString(), hcpUniqueId.get("value").getAsString());
                                           
                                    }
                                    Iterator<JsonElement> it = crosswalks.iterator();
                                    boolean entityForOutput = false;
                                    List<String> crosswalkList = Lists.newArrayList();
                                    while (it.hasNext()) {
                                        JsonElement crosswalk = it.next();
                                        JsonObject crosswalkObject = crosswalk.getAsJsonObject();
                                        String crosswalkUri = crosswalkObject.get("uri").getAsString();
                                        String crosswalkValue = crosswalkObject.get("value").getAsString();
                                        String crosswalkType = crosswalkObject.get("type").getAsString();
                                        JsonArray cwAttributes = crosswalk.getAsJsonObject().get("attributes").getAsJsonArray();
                                        Iterator<JsonElement> attrItr = cwAttributes.iterator();
                                        while (attrItr.hasNext()) {
                                            String attributeUri = attrItr.next().getAsString();
                                            if (attributeUri.contains("HCPUniqueId")) {
                                                if (hcpUniqueCrosswalkMap.containsKey(attributeUri)) {
                                                    System.out.println("Duplicate attribute found for "+attributeUri);
                                                    List<String> cws = duplicateCws.getOrDefault(attributeUri, new ArrayList<>());
                                                    cws.add(crosswalkUri);
                                                    duplicateCws.put(attributeUri, cws);
                                                } else {
                                                    hcpUniqueCrosswalkMap.put(attributeUri, crosswalkUri);
                                                    List<String> cws = duplicateCws.getOrDefault(attributeUri, new ArrayList<>());
                                                    cws.add(crosswalkUri);
                                                    duplicateCws.put(attributeUri, cws);
                                                }
                                            }
                                        }
                                    }
                                    for(Map.Entry<String,List<String>> duplicate : duplicateCws.entrySet()) {
                                        if (duplicate.getValue().size() > 1) {
                                            foundCount.getAndIncrement();
                                            String entityUri = "entities/"+duplicate.getKey().split("/")[1]; 
                                            String crosswalkListAsString = duplicate.getValue().stream().collect(Collectors.joining(","));
                                            b.write(entityUri+",HCPUniqueId="+map.get(duplicate.getKey())+",crosswalks=["+crosswalkListAsString+"]");
                                            b.newLine();
                                            b.flush();
                                        }
                                    }
                                }
                            } catch (Exception ex){
                                System.out.println("Exception : "+ex);
                        }});
                    } else {
                        break;
                    }
                    scans++;
                }
        executorService.awaitTermination(5, TimeUnit.MINUTES);
        executorService.shutdown();
        b.close();
        System.out.println("Process Ended..");
        System.out.println("Total entities scanned: "+entityCount);
        System.out.println("Total entities with duplicate HCPUniqueId in crosswalk attributes: "+foundCount);
        long endTime = System.currentTimeMillis();
        long hours = TimeUnit.MILLISECONDS.toHours(endTime - startTime);
        System.out.println("Total time took: "+hours+" hrs");
    }
}
