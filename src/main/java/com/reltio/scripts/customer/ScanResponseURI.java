package com.reltio.scripts.customer;

import java.util.List;

public class ScanResponseURI {

    public Cursor cursor;
    private List<Uri> objects;

	/*private Attribute cursor;
    public Attribute getCursor() {
		return cursor;
	}
	public void setCursor(Attribute cursor) {
		this.cursor = cursor;
	}*/

    public List<Uri> getObjects() {
        return objects;
    }

    public void setObjects(List<Uri> objects) {
        this.objects = objects;
    }

}
